import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - TI',
    title: 'SmartShop',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "@/assets/global.css",
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/firebase',
    'cookie-universal-nuxt',
    '@nuxtjs/dotenv'
  ],

  firebase: {
    config: {
      apiKey: process.env.apiKey,

      authDomain: process.env.authDomain,

      projectId: process.env.projectId,

      storageBucket: process.env.storageBucket,

      messagingSenderId: process.env.messagingSenderId,

      appId: process.env.appId
    },
    services: {
      auth: {
        persistence: 'local', // default
        initialize: {
          // onAuthStateChangedMutation: 'ON_AUTH_STATE_CHANGED_MUTATION',
          onAuthStateChangedAction: 'onAuthStateChangedAction',
          // subscribeManually: false,
        },
        ssr: false, // default
        emulatorPort: 9099,
        emulatorHost: 'http://localhost',
      },
      firestore: {
        memoryOnly: false, // default
        enablePersistence: true,
        emulatorPort: 8080,
        emulatorHost: 'localhost',
      }, // Just as example. Can be any other service.
      storage: true,
    }
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: "#253d5B",
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,

  
        },
        light: {
          deepblue:"#253d5B",
          deepgrey:"#67697C",
          lightgrey:"#969696",
          deepbrown:"#B79D94",
          deepred:"#C6878F",
          lightbrown:"#e7cbc1",
          deepwhite:"#EFE4E4",
          lightwhite:"#F8EDED",
          myblack:"#666666"
        }
      }
    }
  },

  router: {
    middleware: ['auth'],
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
