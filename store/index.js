export const ACTIONS = {
    ADD_USER_METHOD: 'addUser',
    SIGN_OUT_METHOD: 'signOut',
    SIGN_IN_METHOD: 'signIn',
}

export const state = () => ({
    user: {
        uid: '',
        email: '',
    },
    isConnected: false,
})

export const mutations = {
    SET_USER: (state, { uid, email }) => {
        state.user = { uid, email }, 
        state.isConnected = true
    },

    CLEAR_USER: (state) => {state.user = {}, state.isConnected = false},

    ADD_USER: (state, data) => {
        state.users.push(data)
        state.currentUser = data
    },
    SIGN_OUT: (state) => {
        state.currentUser = {}
    },
    SIGN_IN: (state, data) => {
        const user = state.users.find(
            (user) => user.email == data.email && user.password == data.password
        )

        console.log(user)

        state.currentUser = user ?? {}
    },
}

export const actions = {
    onAuthStateChangedAction ({ commit }, { authUser, claims }) {
        if (!authUser) {
            this.$cookies.remove("user")
            commit('CLEAR_USER')
            console.log("Déconnecté!")

            return
        }

        const { uid, email } = authUser

        commit('SET_USER', { uid, email })
        console.log("Connecté!")

        //await this.dispatch()
    },

    async addUser({ commit }, params) {
        const { email, password } = params

        try {
            const userCreditential = await this.$fire.auth.createUserWithEmailAndPassword(
                email,
                password
            )

            const { uid } = userCreditential.user

            this.$cookies.set('user', { uid })

            this.$fire.firestore
            .collection("users")
            .doc(uid)
            .set({
                email: email,
            })

            this.$router.push('/')
            
        } catch (error) {
            throw new Error(error.message)
        }
        //commit('ADD_USER', params)
    },

    async signIn({ commit }, params) {
        try {
            const userCreditential = await this.$fire.auth.signInWithEmailAndPassword(
                params.email,
                params.password
            )
    
            const { uid } = userCreditential.user
            
            this.$cookies.set('user', { uid })

        } catch (e) {
            throw new Error(e.message)
        }
    },

    signOut({ commit }) {

        this.$fire.auth.signOut()
        commit('CLEAR_USER')
      },
}