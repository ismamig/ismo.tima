export const ACTIONS = {
    //ADD_USER_METHOD: 'addUser',
    //GET Lists of User
    GET_LISTS_METHOD: "lists/getLists",
    //SET Lists of User
    SET_LIST_METHOD: "lists/addList",
    //GET items of list
    //SET items and itemsRef in list
}

export const state = () => ({
    my_lists: [],
})

export const mutations = {

    GET_LISTS: (state, my_lists) => {
        state.my_lists = my_lists
    },
    
}

export const actions = {

    //Ajout d'une liste dans la collection users
    addList(params) {
        this.$fire.firestore
            .collection("users")
            .doc(this.$store.state.user.uid)
            .collection("lists")
            .doc()
            .set({
                name: params.name,
                content: {},
            })
    },

    async getLists({ commit }, { uid }) {
        await this.$fire.firestore
            .collection("users")
            .doc(uid)
            .collection("lists")
            .get()
            .then(
                (lists) => this.my_lists = lists.docs.map((list) => list.data())
            )
        //console.log(this.my_lists)
        
        commit('GET_LISTS', this.my_lists)
    }
}