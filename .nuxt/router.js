import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

<<<<<<< HEAD
const _07bcd644 = () => interopDefault(import('../pages/auth/index.vue' /* webpackChunkName: "pages/auth/index" */))
const _4c2286de = () => interopDefault(import('../pages/inspire.vue' /* webpackChunkName: "pages/inspire" */))
const _a02ffde2 = () => interopDefault(import('../pages/lists/index.vue' /* webpackChunkName: "pages/lists/index" */))
const _6802c77c = () => interopDefault(import('../pages/register.vue' /* webpackChunkName: "pages/register" */))
const _0fe14d1c = () => interopDefault(import('../pages/auth/list.vue' /* webpackChunkName: "pages/auth/list" */))
const _a39d4072 = () => interopDefault(import('../pages/lists/_slug.vue' /* webpackChunkName: "pages/lists/_slug" */))
const _3ff9ae64 = () => interopDefault(import('../pages/lists/_uid.vue' /* webpackChunkName: "pages/lists/_uid" */))
const _3af39b6e = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _3e60ddfe = () => interopDefault(import('../pages/_slug.vue' /* webpackChunkName: "pages/_slug" */))
=======
const _eaf7396e = () => interopDefault(import('..\\pages\\auth\\index.vue' /* webpackChunkName: "pages/auth/index" */))
const _37ca96f9 = () => interopDefault(import('..\\pages\\inspire.vue' /* webpackChunkName: "pages/inspire" */))
const _a2cadbe4 = () => interopDefault(import('..\\pages\\lists\\index.vue' /* webpackChunkName: "pages/lists/index" */))
const _74bb8bd8 = () => interopDefault(import('..\\pages\\register.vue' /* webpackChunkName: "pages/register" */))
const _35218b77 = () => interopDefault(import('..\\pages\\auth\\list.vue' /* webpackChunkName: "pages/auth/list" */))
const _a6381e74 = () => interopDefault(import('..\\pages\\lists\\_slug.vue' /* webpackChunkName: "pages/lists/_slug" */))
const _eb7cfcf6 = () => interopDefault(import('..\\pages\\lists\\_uid.vue' /* webpackChunkName: "pages/lists/_uid" */))
const _4a34f29e = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))
const _4da2352e = () => interopDefault(import('..\\pages\\_slug.vue' /* webpackChunkName: "pages/_slug" */))
>>>>>>> origin/front

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/auth",
    component: _eaf7396e,
    name: "auth"
  }, {
    path: "/inspire",
<<<<<<< HEAD
    component: _4c2286de,
    name: "inspire"
  }, {
    path: "/lists",
    component: _a02ffde2,
=======
    component: _37ca96f9,
    name: "inspire"
  }, {
    path: "/lists",
    component: _a2cadbe4,
>>>>>>> origin/front
    name: "lists"
  }, {
    path: "/register",
    component: _6802c77c,
    name: "register"
  }, {
    path: "/auth/list",
    component: _35218b77,
    name: "auth-list"
  }, {
    path: "/lists/:slug",
    component: _a6381e74,
    name: "lists-slug"
  }, {
    path: "/lists/:uid",
<<<<<<< HEAD
    component: _3ff9ae64,
    name: "lists-uid"
  }, {
    path: "/",
    component: _3af39b6e,
=======
    component: _eb7cfcf6,
    name: "lists-uid"
  }, {
    path: "/",
    component: _4a34f29e,
>>>>>>> origin/front
    name: "index"
  }, {
    path: "/:slug",
    component: _4da2352e,
    name: "slug"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
