<<<<<<< HEAD
export { default as Lists } from '../../components/Lists.vue'
export { default as NuxtLogo } from '../../components/NuxtLogo.vue'
export { default as RegisterForm } from '../../components/RegisterForm.vue'
export { default as Share } from '../../components/Share.vue'
export { default as Tutorial } from '../../components/Tutorial.vue'
export { default as VuetifyLogo } from '../../components/VuetifyLogo.vue'
export { default as ListDetail } from '../../components/listDetail.vue'
export { default as NewItem } from '../../components/newItem.vue'
export { default as NewList } from '../../components/newList.vue'
=======
export { default as ListDetail } from '../..\\components\\listDetail.vue'
export { default as Lists } from '../..\\components\\Lists.vue'
export { default as NewItem } from '../..\\components\\newItem.vue'
export { default as NewList } from '../..\\components\\newList.vue'
export { default as NuxtLogo } from '../..\\components\\NuxtLogo.vue'
export { default as RegisterForm } from '../..\\components\\RegisterForm.vue'
export { default as Share } from '../..\\components\\Share.vue'
export { default as Tutorial } from '../..\\components\\Tutorial.vue'
export { default as VuetifyLogo } from '../..\\components\\VuetifyLogo.vue'
>>>>>>> origin/front

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
