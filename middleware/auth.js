export default function ({ app, route, redirect }) {
    //console.log('middleware auth loaded')
  
    const protectedRoutes = ['index', 'lists', 'lists-slug']
    const guestRoutes = ['auth']
  
    const user = app.$cookies.get('user') ?? {}
    //console.log(user)
    //if (user) {console.log(user.uid)}

    if (user.uid != undefined && guestRoutes.includes(route.name)) {
        return redirect('/')
    }
    
    if (user.uid == undefined && protectedRoutes.includes(route.name)) {
        return redirect('/auth')
    }
}